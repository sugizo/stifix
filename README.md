# Clone the repository
	cd ~/git/gitlab
	git clone git@gitlab.com:sugizo/stifix.git
	
# Create File
	cd ~/git/gitlab/stifix
	
# Add, commit, and push your changes
	git add --all
	git commit -m "Initial Commit"
	git push -u origin master

# Add, commit, and push your changes with tag
	git add --all
	git commit -m "v0.0"
	git tag v0.0
	git tag
	git push -u origin master
	git push origin --tags
